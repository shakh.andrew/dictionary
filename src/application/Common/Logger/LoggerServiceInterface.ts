export default interface LoggerServiceInterface {
  log(...args: any[]): void;
  error(...args: any[]): void;
}
