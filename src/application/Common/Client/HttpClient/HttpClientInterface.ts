import HttpRequestType from './HttpRequestType';

export default interface HttpClientInterface {
  get<T>(url: string, params: HttpRequestType): Promise<T>;

  post(url: string, data: any, config: HttpRequestType);

  put(url: string, data: any, config: HttpRequestType);

  patch(url: string, data: any, config: HttpRequestType);

  delete(url: string, config: HttpRequestType);
}
