type HttpRequestType = {
  url?: string;
  headers?: any;
  params?: any;
  data?: any;
  timeout?: number;
};

export default HttpRequestType;
