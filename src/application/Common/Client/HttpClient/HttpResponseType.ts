import HttpRequestType from './HttpRequestType';

type HttpResponseType = {
  data: any;
  status: number;
  statusText: string;
  headers: any;
  config: HttpRequestType;
  request?: any;
};

export default HttpResponseType;
