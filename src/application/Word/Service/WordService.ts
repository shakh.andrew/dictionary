import WordRepositoryInterface from '../../../domain/Word/WordRepositoryInterface';
import Translation, { AllowedLanguageType } from '../../../domain/Word/Translation';
import Word from '../../../domain/Word/Word';
import NewWordType from '../Type/NewWordType';
import DomainEventPublisher from '../../../domain/SharedKernel/Event/DomainEventPublisher';
import WordCreated from '../../../domain/Word/WordCreated';
import TranslatorClientInterface from '../Type/TranslatorClientInterface';
import WordStatisticsService from '../../../domain/Word/WordStatisticsService';
import WordStatisticsType from '../../../domain/Word/WordStatisticsType';

export default class WordService {
  private wordRepository: WordRepositoryInterface;

  private translatorClient: TranslatorClientInterface;

  private wordStatisticsService: WordStatisticsService;

  constructor(
    wordRepository: WordRepositoryInterface,
    translatorClient: TranslatorClientInterface,
    wordStatisticsService: WordStatisticsService,
  ) {
    this.wordRepository = wordRepository;
    this.translatorClient = translatorClient;
    this.wordStatisticsService = wordStatisticsService;
  }

  public getAll(language: AllowedLanguageType): Promise<Word[]> {
    return this.wordRepository.getBy(language);
  }

  public async getStatistics(language: AllowedLanguageType): Promise<WordStatisticsType> {
    const words = await this.getAll(language);

    return this.wordStatisticsService.getStatistics(words);
  }

  public async add(data: NewWordType): Promise<any> {
    const id = this.wordRepository.getNextId();
    let translation = null;
    try {
      translation = await this.translatorClient.translate(data.origin, 'en', data.target);
    } catch (e) {
      console.log(e);
      translation = Translation.create(data.origin, data.target);
    }
    const word = Word.create(id, data.origin, [translation]);

    await this.wordRepository.add(word);
    DomainEventPublisher.instance().publish(WordCreated.create(id, data.origin));
  }
}
