import DomainSubscriberInterface from '../../../domain/SharedKernel/Event/Types/DomainSubscriberInterface';
import LoggerServiceInterface from '../../Common/Logger/LoggerServiceInterface';
import WordCreated from '../../../domain/Word/WordCreated';

export default class WordLogger implements DomainSubscriberInterface {
  private logger: LoggerServiceInterface;

  constructor(logger: LoggerServiceInterface) {
    this.logger = logger;
  }

  public handleEvent(domainEvent: WordCreated): Promise<void> {
    this.logger.log(`New word "${domainEvent.getOrigin()}" was created with ID: ${domainEvent.getId()}`);

    return Promise.resolve();
  }

  public subscribedToEventType(): string {
    return 'WordCreated';
  }
}
