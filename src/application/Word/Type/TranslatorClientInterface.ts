import Translation, { AllowedLanguageType } from '../../../domain/Word/Translation';

export default interface TranslatorClientInterface {
  translate(word: string, source: string, target: AllowedLanguageType): Promise<Translation>;
}
