import { AllowedLanguageType } from '../../../domain/Word/Translation';

type NewWordType = {
  origin: string;
  target: AllowedLanguageType;
};

export default NewWordType;
