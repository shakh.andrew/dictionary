import WordRepository from './Word/WordRepository';
import InMemoryPersistence from './Common/Db/InMemoryPersistence';
import WordInitData from './Data/WordInitData';
import WordService from '../application/Word/Service/WordService';
import GoogleTranslatorClient from './Word/Translator/Provider/Google/GoogleTranslatorClient';
import AxiosHttpClient from './Common/Client/AxiosHttpClient';
import WordStatisticsService from '../domain/Word/WordStatisticsService';

export default class DomainRegistry {
  public static getWordService(): WordService {
    return new WordService(this.getWordRepository(), this.getGoogleTranslatorClient(), new WordStatisticsService());
  }

  private static getWordRepository(): WordRepository {
    const persistence = InMemoryPersistence.create(WordRepository.collection, WordInitData);

    return new WordRepository(persistence);
  }

  private static getGoogleTranslatorClient(): GoogleTranslatorClient {
    return new GoogleTranslatorClient(this.getAxiosHttpClient(), 'qwerty');
  }

  private static getAxiosHttpClient(): AxiosHttpClient {
    return new AxiosHttpClient();
  }
}
