import express from 'express';
import bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';
import path from 'path';
import YAML from 'yamljs';
import ErrorHandler from './utils/ErrorHandler';
import WordRouter from './Word/WordRouter';
import DomainSubscriber from '../Common/DomainSubscriber';

const swaggerFile = path.resolve('src/infrastructure/api/swagger.yml');
const swaggerDocument = YAML.load(swaggerFile);

export default class App {
  private static app: express.Application;

  public static async getApplication(): Promise<express.Application> {
    if (!App.app) {
      App.app = await App.init();
    }

    return Promise.resolve(App.app);
  }

  private static async init(): Promise<express.Application> {
    DomainSubscriber.register();

    const app = express();

    app.set('port', process.env.PORT || 3000);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    const workRouter = new WordRouter();
    app.use('/api', workRouter.getRouter());
    app.use('/api', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    app.use(ErrorHandler.catchNotFound);
    app.use(ErrorHandler.catchError);

    return app;
  }

  public async start(): Promise<void> {
    const app = await App.getApplication();

    app.listen(app.get('port'), () => {
      console.log('*** App is running at http://localhost:%d in %s mode', app.get('port'), app.get('env'));
      console.log('*** Press CTRL-C to stop\n');
    });
  }
}
