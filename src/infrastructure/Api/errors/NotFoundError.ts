import httpStatus from 'http-status';
import AppError from './AppError';

export default class NotFoundError extends AppError {
  status = httpStatus.NOT_FOUND;
}
