import httpStatus from 'http-status';
import AppError from './AppError';

export default class ValidationError extends AppError {
  status = httpStatus.BAD_REQUEST;
}
