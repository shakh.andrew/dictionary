export default abstract class AppError extends Error {
  public abstract status: number;
}
