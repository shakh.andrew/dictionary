import { Router } from 'express';

export default abstract class AppRouter {
  abstract getRouter(): Router;
}
