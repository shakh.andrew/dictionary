import Word from '../../../domain/Word/Word';
import WordMessageType from './WordMessageType';
import { AllowedLanguageType } from '../../../domain/Word/Translation';

export default class WordAdapter {
  public static toDto(item: Word, language: AllowedLanguageType): WordMessageType {
    return {
      origin: item.getOrigin(),
      translation: item.getTranslation(language),
    };
  }
}
