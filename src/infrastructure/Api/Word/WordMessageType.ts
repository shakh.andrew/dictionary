type WordMessageType = {
  origin: string;
  translation: string;
};

export default WordMessageType;
