import WordController from './WordController';
import DomainRegistry from '../../DomainRegistry';

export default class WordControllerFactory {
  static getController(): WordController {
    return new WordController(DomainRegistry.getWordService());
  }
}
