import { Router } from 'express';
import AppRouter from '../AppRouter';
import WordControllerFactory from './WordControllerFactory';
import WordController from './WordController';

const router = Router();

export default class WordRouter extends AppRouter {
  private wordController: WordController;

  constructor() {
    super();
    this.wordController = WordControllerFactory.getController();
  }

  public getRouter(): Router {
    router.get('/words', (req, res, next) => {
      return this.wordController.getAll(req, res, next);
    });

    router.post('/words', (req, res, next) => {
      return this.wordController.create(req, res, next);
    });

    return router;
  }
}
