import { NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status';
import WordValidator from './WordValidator';
import WordService from '../../../application/Word/Service/WordService';
import WordAdapter from './WordAdapter';

export default class WordController {
  private wordService: WordService;

  constructor(wordService: WordService) {
    this.wordService = wordService;
  }

  public async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      const language = WordValidator.getAll(req.query.language);
      const words = await this.wordService.getAll(language);
      const result = words.map((word) => WordAdapter.toDto(word, language));

      res.status(httpStatus.OK).json(result);
    } catch (e) {
      next(e);
    }
  }

  public async create(req: Request, res: Response, next: NextFunction) {
    try {
      const params = WordValidator.create(req.body);
      await this.wordService.add(params);

      res.status(httpStatus.OK).json({});
    } catch (e) {
      next(e);
    }
  }
}
