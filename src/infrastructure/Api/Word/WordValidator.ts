import Joi from '@hapi/joi';
import ValidationError from '../errors/ValidationError';
import Translation from '../../../domain/Word/Translation';

export default class WordValidator {
  static getAll(language: any) {
    const result = Joi.string().required().valid(...Translation.allowedLanguage).validate(language);
    if (result.error) {
      throw new ValidationError(result.error.message);
    }

    return result.value;
  }

  static create(params: any) {
    const result = Joi.object({
      origin: Joi.string().required(),
      target: Joi.string().required().valid(...Translation.allowedLanguage),
    }).validate(params);
    if (result.error) {
      throw new ValidationError(result.error.message);
    }

    return result.value;
  }
}
