import { NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status';
import ErrorCodes from '../errors/ErrorCodes';
import NotFoundError from '../errors/NotFoundError';

export default class ErrorHandler {
  public static catchNotFound(req: Request, res: Response, next: NextFunction): void {
    next(new NotFoundError(ErrorCodes.NOT_FOUND));
  }

  /* eslint-disable @typescript-eslint/no-unused-vars */
  public static catchError(err: any, req: Request, res: Response, next: NextFunction): void {
    if (err) {
      const statusCode: number = err.status || httpStatus.INTERNAL_SERVER_ERROR;
      res.status(statusCode).json({
        error: err.message,
        details: err.details || undefined,
        statusCode,
      });
    }
  }
}
