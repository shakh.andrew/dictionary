import { GoogleTranslatorResponseType } from './Type/GoogleTranslatorType';
import GoogleTranslatorAdapter from './GoogleTranslatorAdapter';
import TranslatorClientInterface from '../../../../../application/Word/Type/TranslatorClientInterface';
import HttpClientInterface from '../../../../../application/Common/Client/HttpClient/HttpClientInterface';
import Translation from '../../../../../domain/Word/Translation';

export default class GoogleTranslatorClient implements TranslatorClientInterface {
  private httpClient: HttpClientInterface;

  private readonly apiKey: string;

  private readonly timeout: number;

  constructor(httpClient: HttpClientInterface, apiKey: string) {
    this.httpClient = httpClient;
    this.apiKey = apiKey;
    this.timeout = 5000;
  }

  public async translate(word: string, source: string, target: string): Promise<Translation> {
    const url = `https://translation.googleapis.com/language/translate/v2?key=${this.apiKey}&q=${word}&target=${target}&source=${source}`;
    const result: GoogleTranslatorResponseType = await this.httpClient.get(url, { timeout: this.timeout });

    return GoogleTranslatorAdapter.adapt(target, result);
  }
}
