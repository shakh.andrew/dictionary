import { GoogleTranslatorResponseType } from './Type/GoogleTranslatorType';
import Translation from '../../../../../domain/Word/Translation';

export default class GoogleTranslatorAdapter {
  static adapt(target: string, data: GoogleTranslatorResponseType): Translation {
    const translation = data.data.translations[0].translatedText;

    return Translation.create(translation, target);
  }
}
