export type GoogleTranslatorResponseType = {
  data: {
    translations: {
      translatedText: string;
    }[];
  };
};
