type WordPersistenceType = {
  id: string;
  origin: string;
  translations: {
    word: string;
    language: string;
  }[];
};

export default WordPersistenceType;
