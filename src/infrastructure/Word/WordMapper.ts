import WordPersistenceType from './Type/WordPersistenceType';
import Word from '../../domain/Word/Word';
import Translation from '../../domain/Word/Translation';

export default class WordMapper {
  static toPersistence(entity: Word): WordPersistenceType {
    return {
      id: entity.getId(),
      origin: entity.getOrigin(),
      translations: entity.getTranslations().map((translation) => {
        return {
          word: translation.getWord(),
          language: translation.getLang(),
        };
      }),
    };
  }

  static toDomain(item: WordPersistenceType): Word {
    const translations = item.translations.map((translation) => {
      return Translation.create(translation.word, translation.language);
    });

    return Word.create(item.id, item.origin, translations);
  }
}
