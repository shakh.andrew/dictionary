import WordRepositoryInterface from '../../domain/Word/WordRepositoryInterface';
import PersistenceInterface from '../Common/Db/PersistenceInterface';
import Word from '../../domain/Word/Word';
import { AllowedLanguageType } from '../../domain/Word/Translation';
import WordPersistenceType from './Type/WordPersistenceType';
import WordMapper from './WordMapper';

export default class WordRepository implements WordRepositoryInterface {
  private readonly persistence: PersistenceInterface;

  static collection = 'Word';

  constructor(persistence: PersistenceInterface) {
    this.persistence = persistence;
  }

  public add(word: Word): Promise<any> {
    return this.persistence.add<WordPersistenceType>(WordRepository.collection, WordMapper.toPersistence(word));
  }

  public async getBy(language: AllowedLanguageType): Promise<Word[]> {
    const filteredItems = [];
    const items = await this.persistence.getAll<WordPersistenceType>(WordRepository.collection);
    for (const item of items) {
      const translate = item.translations.find((i) => i.language === language);
      if (translate) {
        filteredItems.push(item);
      }
    }

    return Promise.resolve(filteredItems.map(WordMapper.toDomain));
  }

  public getNextId(): string {
    return this.persistence.generateUid();
  }
}
