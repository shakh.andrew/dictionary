export default [
  {
    id: '111',
    origin: 'dog',
    translations: [
      {
        word: 'собака',
        language: 'uk',
      },
    ],
  },
  {
    id: '222',
    origin: 'cat',
    translations: [
      {
        word: 'кіт',
        language: 'uk',
      },
      {
        word: 'chatte',
        language: 'fr',
      },
    ],
  },
];
