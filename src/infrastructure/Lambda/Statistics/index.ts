import DomainRegistry from '../../DomainRegistry';

exports.handler = async () => {
  try {
    const wordService = DomainRegistry.getWordService();

    return await wordService.getStatistics('uk');
  } catch (error) {
    console.log(error);
    throw error;
  }
};
