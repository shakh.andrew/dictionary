import axios, { AxiosStatic } from 'axios';
import HttpClientInterface from '../../../application/Common/Client/HttpClient/HttpClientInterface';
import HttpRequestType from '../../../application/Common/Client/HttpClient/HttpRequestType';
import HttpResponseType from '../../../application/Common/Client/HttpClient/HttpResponseType';

export default class AxiosHttpClient implements HttpClientInterface {
  private readonly axios: AxiosStatic;

  constructor() {
    this.axios = axios;
  }

  public get<T>(url: string, config?: HttpRequestType): Promise<T> {
    return this.axios.get(url, config).then(resp => {
      return resp.data;
    });
  }

  public post(url: string, data?: any, config?: HttpRequestType): Promise<HttpResponseType> {
    return this.axios.post(url, data, config);
  }

  public put(url: string, data?: any, config?: HttpRequestType): Promise<HttpResponseType> {
    return this.axios.put(url, data, config);
  }

  public patch(url: string, data?: any, config?: HttpRequestType): Promise<HttpResponseType> {
    return this.axios.patch(url, data, config);
  }

  public delete(url: string, config?: HttpRequestType): Promise<HttpResponseType> {
    return this.axios.delete(url, config);
  }
}
