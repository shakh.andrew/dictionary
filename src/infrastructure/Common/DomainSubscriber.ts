import DomainEventPublisher from '../../domain/SharedKernel/Event/DomainEventPublisher';
import WordLogger from '../../application/Word/DomainSubscriber/WordLogger';
import LoggerService from './Service/LoggerService';

export default class DomainSubscriber {
  static register(): void {
    const wordLogger = new WordLogger(new LoggerService());
    DomainEventPublisher.instance().subscribe(wordLogger);
  }
}
