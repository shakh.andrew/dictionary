export default interface PersistenceInterface {
  getAll<T>(collection: string): Promise<T[]>;
  add<T>(collection: string, data: T): Promise<void>;
  delete(key: string): Promise<void>;
  generateUid(): string;
}
