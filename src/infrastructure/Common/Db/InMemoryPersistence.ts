import * as uuid from 'uuid';
import PersistenceInterface from './PersistenceInterface';

export default class InMemoryPersistence implements PersistenceInterface {
  private static instance: InMemoryPersistence = null;

  private readonly data = new Map<string, any[]>();

  private constructor(collection: string, data: any[]) {
    this.data.set(collection, data);
  }

  static create(collection: string, data: any[]) {
    if (this.instance === null) {
      this.instance = new InMemoryPersistence(collection, data);
    }

    return this.instance;
  }

  public getAll<T>(collection: string): Promise<T[]> {
    const item = this.data.get(collection) ?? null;

    return Promise.resolve(item);
  }

  public add<T>(collection: string, item: T): Promise<void> {
    console.log('DB.set => ', collection, item);
    const items = this.data.get(collection);
    items.push(item);
    this.data.set(collection, items);

    return Promise.resolve();
  }

  public delete(collection: string): Promise<void> {
    this.data.delete(collection);

    return Promise.resolve();
  }

  public generateUid(): string {
    return uuid.v4();
  }
}
