import LoggerServiceInterface from '../../../application/Common/Logger/LoggerServiceInterface';

export default class LoggerService implements LoggerServiceInterface {
  public log(...args: any[]): void {
    console.log(args);
  }

  public error(...args: any[]): void {
    console.error(args);
  }
}
