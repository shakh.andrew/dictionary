import ValueObject from '../SharedKernel/ValueObject';

export type AllowedLanguageType = typeof Translation.allowedLanguage[number];

export default class Translation extends ValueObject {
  public static allowedLanguage = ['es', 'uk', 'fr'];

  private readonly word: string;

  private readonly lang: AllowedLanguageType;

  private constructor(word: string, lang: AllowedLanguageType) {
    super();

    this.assertArgumentNotEmpty(word, 'Translation word should not be empty');
    this.assertArgumentAllowed(
      lang,
      Translation.allowedLanguage,
      `Only allowed following languages: ${Translation.allowedLanguage.join(',')}`,
    );

    this.word = word;
    this.lang = lang;
  }

  public static create(word: string, lang: AllowedLanguageType): Translation {
    return new Translation(word, lang);
  }

  public getWord(): string {
    return this.word;
  }

  public getLang(): AllowedLanguageType {
    return this.lang;
  }

  protected getHash(): string {
    return `${this.lang}_${this.word}`;
  }
}
