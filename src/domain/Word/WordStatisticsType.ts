type WordStatisticsType = {
  countOfWords: number;
  countOfLetters: number;
};

export default WordStatisticsType;
