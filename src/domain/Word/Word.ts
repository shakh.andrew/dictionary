import Entity from '../SharedKernel/Entity';
import Translation, { AllowedLanguageType } from './Translation';

export default class Word extends Entity {
  private readonly id: string = '';

  private readonly origin: string = '';

  private readonly translations: Translation[] = [];

  private constructor(id: string, origin: string, translations: Translation[]) {
    super();

    this.assertArgumentNotEmpty(id, 'id should not be empty');
    this.assertArgumentNotEmpty(origin, 'origin should not be empty');
    this.assertArgumentNotEmptyArray(translations, 'translations should not be empty array');

    this.id = id;
    this.origin = origin;
    this.translations = translations;
  }

  public static create(id: string, origin: string, translations: Translation[]): Word {
    return new Word(id, origin, translations);
  }

  public getId(): string {
    return this.id;
  }

  public getOrigin(): string {
    return this.origin;
  }

  public getTranslations(): Translation[] {
    return this.translations;
  }

  public getTranslation(lang: AllowedLanguageType): string {
    return this.translations.find((translation) => translation.getLang() === lang)?.getWord();
  }

  public countOfLetters(): number {
    return this.origin.length;
  }
}
