import Word from './Word';
import { AllowedLanguageType } from './Translation';

export default interface WordRepositoryInterface {
  getNextId(): string;
  getBy(language: AllowedLanguageType): Promise<Word[]>;
  add(word: Word): Promise<any>;
}
