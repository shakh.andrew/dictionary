import DomainEvent from '../SharedKernel/Event/DomainEvent';

export default class WordCreated extends DomainEvent {
  private readonly id: string = '';

  private readonly origin: string = '';

  private constructor(id: string, origin: string) {
    super();
    this.id = id;
    this.origin = origin;
  }

  public static create(id: string, origin: string) {
    return new WordCreated(id, origin);
  }

  public getId(): string {
    return this.id;
  }

  public getOrigin(): string {
    return this.origin;
  }
}
