import Word from './Word';
import WordStatisticsType from './WordStatisticsType';

export default class WordStatisticsService {
  public getStatistics(words: Word[]): WordStatisticsType {
    const countOfWords = words.length;
    const countOfLetters = words.reduce((prev, current) => prev + current.countOfLetters(), 0);

    return {
      countOfWords,
      countOfLetters,
    };
  }
}
