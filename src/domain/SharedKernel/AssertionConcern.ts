import IllegalArgumentException from './Error/IllegalArgumentException';

export default abstract class AssertionConcern {
  protected assertArgumentMaxLength(string: string, maximum: number, message: string): void {
    const length = string.trim().length;
    if (length > maximum) {
      throw new IllegalArgumentException(message);
    }
  }

  protected assertArgumentLength(string: string, minimum: number, maximum: number, message: string): void {
    const length = string.trim().length;
    if (length < minimum || length > maximum) {
      throw new IllegalArgumentException(message);
    }
  }

  protected assertArgumentNotEmpty(value: string, message: string): void {
    if (value === null || value === undefined || value.trim() === '') {
      throw new IllegalArgumentException(message);
    }
  }

  protected assertArgumentExistAndNotNull(value: any, message: string): void {
    if (value === null || value === undefined) {
      throw new IllegalArgumentException(message);
    }
  }

  protected assertArgumentRange(value: number, minimum: number, maximum: number, message: string): void {
    if (value < minimum || value > maximum) {
      throw new IllegalArgumentException(message);
    }
  }

  protected assertArgumentTrue(boolean: boolean, message: string): void {
    if (!boolean) {
      throw new IllegalArgumentException(message);
    }
  }

  protected assertArgumentFalse(boolean: boolean, message: string): void {
    if (boolean) {
      throw new IllegalArgumentException(message);
    }
  }

  protected assertArgumentAllowed(value: any, allowed: any[], message: string): void {
    if (!allowed.includes(value)) {
      throw new IllegalArgumentException(message);
    }
  }

  protected assertArgumentNotEmptyArray(value: any[], message: string): void {
    if (value.length === 0) {
      throw new IllegalArgumentException(message);
    }
  }
}
