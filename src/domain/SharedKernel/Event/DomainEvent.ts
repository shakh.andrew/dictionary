import DomainEventInterface from './Types/DomainEventInterface';

export default abstract class DomainEvent implements DomainEventInterface {
  private readonly OccurredOn;

  protected constructor() {
    this.OccurredOn = Date.now();
  }

  public name(): string {
    return this.constructor.name;
  }

  public occurredOn(): number {
    return this.OccurredOn;
  }
}
