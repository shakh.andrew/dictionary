export default interface DomainSubscriberInterface {
  handleEvent(domainEvent): Promise<void>;
  subscribedToEventType(): string
}
