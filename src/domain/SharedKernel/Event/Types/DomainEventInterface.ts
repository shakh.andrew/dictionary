export default interface DomainEventInterface {
  occurredOn(): number;
  name(): string;
}
