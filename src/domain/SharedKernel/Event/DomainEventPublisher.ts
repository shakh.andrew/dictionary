import DomainEventInterface from './Types/DomainEventInterface';
import DomainSubscriberInterface from './Types/DomainSubscriberInterface';

export default class DomainEventPublisher {
  private static Instance: DomainEventPublisher = null;

  private Subscribers: Set<DomainSubscriberInterface>;

  private constructor() {
    this.reset();
  }

  public static instance(): DomainEventPublisher {
    if (this.Instance === null) {
      this.Instance = new DomainEventPublisher();
    }

    return this.Instance;
  }

  public publish(domainEvent: DomainEventInterface): void {
    const eventName = domainEvent.name();
    const subscribers = this.subscribersByEventName(eventName);

    if (subscribers.length > 0) {
      const promises = subscribers.map(subscriber => subscriber.handleEvent(domainEvent));
      Promise.all(promises).then(() => {
        console.log(`All subscribers were invoked for ${eventName}`);
      });
    }
  }

  public publishAll(domainEvents: DomainEventInterface[]): void {
    for (const domainEvent of domainEvents) {
      this.publish(domainEvent);
    }
  }

  public reset(): void {
    this.Subscribers = new Set<DomainSubscriberInterface>();
  }

  public subscribe(subscriber: DomainSubscriberInterface): void {
    this.Subscribers.add(subscriber);
  }

  private subscribersByEventName(eventName: string): DomainSubscriberInterface[] {
    return this.subscribers().filter(subscriber => subscriber.subscribedToEventType() === eventName);
  }

  private subscribers(): any {
    return Array.from(this.Subscribers);
  }
}
