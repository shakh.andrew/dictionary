import AssertionConcern from './AssertionConcern';

export default abstract class ValueObject extends AssertionConcern {
  public equals(object: ValueObject) {
    if (object === undefined) {
      return false;
    }

    return this.getHash() === object.getHash();
  }

  protected abstract getHash(): string;
}
