export default class Helper {
  static clone(obj: any) {
    return JSON.parse(JSON.stringify(obj));
  }

  static mockDate(year = 2020, month = 11, day = 15) {
    jest.spyOn(Date, 'now').mockImplementation(() => new Date(Date.UTC(year, month - 1, day, 0, 0, 0)).valueOf());
  }

  static mockRandom(val = 0.123456789) {
    jest.spyOn(global.Math, 'random').mockReturnValue(val);
  }

  static reset() {
    jest.spyOn(global.Math, 'random').mockRestore();
    jest.spyOn(Date, 'now').mockRestore();
  }
}
