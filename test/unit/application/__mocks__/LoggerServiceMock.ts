import LoggerServiceInterface from '../../../../src/application/Common/Logger/LoggerServiceInterface';

export const mockLog = jest.fn();

export const mockError = jest.fn();

export default class LoggerServiceMock implements LoggerServiceInterface {
  public log(...args: any[]): void {
    return mockLog(args);
  }

  public error(...args: any[]): void {
    return mockError(args);
  }
}
