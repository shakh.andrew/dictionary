import Translation from '../../../../src/domain/Word/Translation';
import Word from '../../../../src/domain/Word/Word';

export class WordTest {
  static getWord(id = '111-222-333'): Word {
    const translation = WordTest.getTranslation();

    return Word.create(id, 'dog', [translation]);
  }

  static getTranslation(): Translation {
    return Translation.create('собака', 'uk');
  }
}
