import TranslatorClientInterface from '../../../../src/application/Word/Type/TranslatorClientInterface';
import Translation, { AllowedLanguageType } from '../../../../src/domain/Word/Translation';
import { WordTest } from './WordTest';

export const mockTranslate = jest.fn((word, source, target) => {
  console.log(word, source, target);

  return Promise.resolve(WordTest.getTranslation());
});

export default class TranslatorClientMock implements TranslatorClientInterface {
  public translate(word: string, source: string, target: AllowedLanguageType): Promise<Translation> {
    return mockTranslate(word, source, target);
  }
}
