import WordRepositoryInterface from '../../../../src/domain/Word/WordRepositoryInterface';
import Word from '../../../../src/domain/Word/Word';
import { AllowedLanguageType } from '../../../../src/domain/Word/Translation';
import { WordTest } from './WordTest';

const word1 = WordTest.getWord('111');
const word2 = WordTest.getWord('222');

export const mockAdd = jest.fn((word) => Promise.resolve(word));
export const mockGetBy = jest.fn((lang) => {
  console.log(lang);

  return Promise.resolve([word1, word2]);
});
export const mockGetNextId = jest.fn(() => '111-222-333');

export default class WordRepositoryMock implements WordRepositoryInterface {
  public add(word: Word): Promise<any> {
    return mockAdd(word);
  }

  public getBy(language: AllowedLanguageType): Promise<Word[]> {
    return mockGetBy(language);
  }

  public getNextId(): string {
    return mockGetNextId();
  }
}
