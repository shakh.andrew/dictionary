import WordService from '../../../../../src/application/Word/Service/WordService';
import WordRepositoryMock, { mockGetNextId, mockAdd, mockGetBy } from '../../__mocks__/WordRepositoryMock';
import TranslatorClientMock, { mockTranslate } from '../../__mocks__/TranslatorClientMock';
import { WordTest } from '../../__mocks__/WordTest';
import WordStatisticsService from '../../../../../src/domain/Word/WordStatisticsService';

describe('WordService unit test', () => {
  let wordService: WordService = null;

  beforeEach(() => {
    wordService = new WordService(new WordRepositoryMock(), new TranslatorClientMock(), new WordStatisticsService());
  });

  it('should return correct data', async () => {
    const items = await wordService.getAll('uk');

    expect(items).toHaveLength(2);
    expect(mockGetBy).toBeCalled();
    expect(mockGetBy).toBeCalledWith('uk');
  });

  it('should save correct data', async () => {
    await wordService.add({
      origin: 'dog',
      target: 'uk',
    });

    expect(mockGetNextId).toBeCalled();

    expect(mockAdd).toBeCalledTimes(1);
    expect(mockAdd).toBeCalledWith(WordTest.getWord());

    expect(mockTranslate).toBeCalledTimes(1);
    expect(mockTranslate).toBeCalledWith('dog', 'en', 'uk');
  });

  it('should return right statistics data', async () => {
    const data = await wordService.getStatistics('uk');

    expect(data.countOfWords).toEqual(2);
    expect(data.countOfLetters).toEqual(6);
  });
});
