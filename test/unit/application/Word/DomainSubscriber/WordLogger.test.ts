import WordLogger from '../../../../../src/application/Word/DomainSubscriber/WordLogger';
import WordCreated from '../../../../../src/domain/Word/WordCreated';
import LoggerServiceMock, { mockLog } from '../../__mocks__/LoggerServiceMock';

describe('WordLogger unit test', () => {
  let wordLogger: WordLogger = null;
  const domainEvent = WordCreated.create('111-222-333', 'dog');

  beforeEach(() => {
    wordLogger = new WordLogger(new LoggerServiceMock());
  });

  it('should subscribe on right event', async () => {
    expect(wordLogger.subscribedToEventType()).toEqual('WordCreated');
  });

  it('should log event data', async () => {
    await wordLogger.handleEvent(domainEvent);

    expect(mockLog).toBeCalled();
    expect(mockLog).toBeCalledWith(['New word "dog" was created with ID: 111-222-333']);
  });
});
