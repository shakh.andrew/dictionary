import GoogleTranslatorClient from '../../../../../../../src/infrastructure/Word/Translator/Provider/Google/GoogleTranslatorClient';
import HttpClientStub from '../../../../__mocks__/HttpClientStub';
import Translation from '../../../../../../../src/domain/Word/Translation';

describe('GoogleTranslatorClient unit test', () => {
  let client: GoogleTranslatorClient;
  const translation = Translation.create('пес', 'uk');
  const httpClientStub = new HttpClientStub();
  const mockResponse = {
    data: {
      translations: [
        {
          translatedText: translation.getWord(),
        },
      ],
    },
  };

  beforeEach(() => {
    client = new GoogleTranslatorClient(httpClientStub, '123456');
  });

  it('should call axios get method', async () => {
    const url = 'https://translation.googleapis.com/language/translate/v2?key=123456&q=dog&target=uk&source=en';
    httpClientStub.setResponse('get', url, mockResponse);

    const responseTranslation = await client.translate('dog', 'en', 'uk');
    expect(responseTranslation.equals(translation)).toBeTruthy();
  });
});
