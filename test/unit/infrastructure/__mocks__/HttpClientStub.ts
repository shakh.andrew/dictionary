import HttpClientInterface from '../../../../src/application/Common/Client/HttpClient/HttpClientInterface';
import HttpRequestType from '../../../../src/application/Common/Client/HttpClient/HttpRequestType';

export default class HttpClientStub implements HttpClientInterface {
  private response: { [key in keyof HttpClientInterface]: any } = {
    get: new Map(),
    post: new Map(),
    put: new Map(),
    patch: new Map(),
    delete: new Map(),
  };

  private methodParams: any;

  public setResponse(method: keyof HttpClientInterface, url: string, response: any) {
    this.response[method].set(url, response);
  }

  public get<T>(url: string, params: HttpRequestType): Promise<T> {
    this.setMethodParams({ url, params });

    return this.getResponse('get', url);
  }

  public delete(url: string, config: HttpRequestType) {
    this.setMethodParams({ url, config });

    return this.getResponse('delete', url);
  }

  public patch(url: string, data: any, config: HttpRequestType) {
    this.setMethodParams({ url, data, config });

    return this.getResponse('patch', url);
  }

  public post(url: string, data: any, config: HttpRequestType) {
    this.setMethodParams({ url, data, config });

    return this.getResponse('post', url);
  }

  public put(url: string, data: any, config: HttpRequestType) {
    this.setMethodParams({ url, data, config });

    return this.getResponse('put', url);
  }

  public getMethodParams(): any {
    return this.methodParams;
  }

  private setMethodParams(params): void {
    this.methodParams = params;
  }

  private getResponse(method: keyof HttpClientInterface, url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const response = this.response[method].get(url);

      if (response instanceof Error) {
        reject(response);
      }

      resolve(response);
    });
  }
}
