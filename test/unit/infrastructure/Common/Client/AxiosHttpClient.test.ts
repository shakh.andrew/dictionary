import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import AxiosHttpClient from '../../../../../src/infrastructure/Common/Client/AxiosHttpClient';

const axiosMock = new MockAdapter(axios);
const baseUrl = 'http://test.com/';

describe('AxiosHttpClient unit test', () => {
  it('should call axios get method', async () => {
    expect.assertions(2);

    axiosMock.onGet(baseUrl).replyOnce((config) => {
      expect(config.method).toEqual('get');

      return [200, { test: 123 }];
    });

    const axiosHttpClient = new AxiosHttpClient();
    const response = await axiosHttpClient.get(baseUrl);
    expect(response).toEqual({ test: 123 });
  });

  it('should call axios post method', async () => {
    expect.assertions(2);
    const data = { test: '123' };

    axiosMock.onPost(baseUrl).replyOnce((config) => {
      expect(config.method).toEqual('post');
      expect(config.data).toEqual(JSON.stringify(data));

      return [200, {}];
    });

    const axiosHttpClient = new AxiosHttpClient();
    await axiosHttpClient.post(baseUrl, data);
  });

  it('should call axios put method', async () => {
    expect.assertions(2);
    const data = { test: '123' };

    axiosMock.onPut(baseUrl).replyOnce((config) => {
      expect(config.method).toEqual('put');
      expect(config.data).toEqual(JSON.stringify(data));

      return [200, {}];
    });

    const axiosHttpClient = new AxiosHttpClient();
    await axiosHttpClient.put(baseUrl, data);
  });

  it('should call axios patch method', async () => {
    expect.assertions(2);
    const data = { test: '123' };

    axiosMock.onPatch(baseUrl).replyOnce((config) => {
      expect(config.method).toEqual('patch');
      expect(config.data).toEqual(JSON.stringify(data));

      return [200, {}];
    });

    const axiosHttpClient = new AxiosHttpClient();
    await axiosHttpClient.patch(baseUrl, data);
  });

  it('should call axios delete method', async () => {
    expect.assertions(1);
    axiosMock.onDelete(baseUrl).replyOnce((config) => {
      expect(config.method).toEqual('delete');

      return [200, {}];
    });

    const axiosHttpClient = new AxiosHttpClient();
    await axiosHttpClient.delete(baseUrl);
  });

  it('should throw timeout error', async () => {
    axiosMock.onAny().timeout();

    const axiosHttpClient = new AxiosHttpClient();
    await expect(axiosHttpClient.delete(baseUrl, { timeout: 500 })).rejects.toThrowError('timeout of 500ms exceeded');
  });
});
