export default {
  word: {
    id: '111-222-333',
    origin: 'dog',
    translations: [
      { word: 'собака', lang: 'uk' },
      { word: 'chienne', lang: 'fr' },
    ],
  },
};
