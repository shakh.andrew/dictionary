import DomainEvent from '../../../../../../src/domain/SharedKernel/Event/DomainEvent';

export default class EventA extends DomainEvent {
  private readonly id: string;

  private readonly title: string;

  constructor(id: string, title: string) {
    super();
    this.id = id;
    this.title = title;
  }

  public getId(): string {
    return this.id;
  }

  public getTitle(): string {
    return this.title;
  }
}
