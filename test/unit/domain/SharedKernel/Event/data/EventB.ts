import DomainEvent from '../../../../../../src/domain/SharedKernel/Event/DomainEvent';

export default class EventB extends DomainEvent {
  private readonly Id: string;

  private readonly Count: number;

  constructor(id: string, count: number) {
    super();
    this.Id = id;
    this.Count = count;
  }

  public id(): string {
    return this.Id;
  }

  public count(): number {
    return this.Count;
  }
}
