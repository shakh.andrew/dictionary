import DomainSubscriberInterface from '../../../../../../src/domain/SharedKernel/Event/Types/DomainSubscriberInterface';
import EventA from './EventA';

export const mockHandleEvent = jest.fn();

export const mockSubscribedToEventType = jest.fn(() => 'EventA');

export default class TestSubscriber implements DomainSubscriberInterface {
  public handleEvent(domainEvent: EventA): Promise<void> {
    return mockHandleEvent(domainEvent);
  }

  public subscribedToEventType(): string {
    return mockSubscribedToEventType();
  }
}
