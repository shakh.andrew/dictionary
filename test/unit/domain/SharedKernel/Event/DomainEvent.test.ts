import EventA from './data/EventA';
import Helper from '../../../../Helper';

describe('DomainEvent abstraction unit test', () => {
  let domainEvent: EventA = null;
  const id = '111-222-333';
  const title = 'test title';
  const eventName = 'EventA';
  const timestamp = 1653177600000;

  beforeEach(() => {
    Helper.mockDate(2022, 5, 22);
    domainEvent = new EventA(id, title);
  });

  afterEach(() => {
    Helper.reset();
  });

  it('should return right init data', async () => {
    expect(domainEvent.getId()).toEqual(id);
    expect(domainEvent.getTitle()).toEqual(title);
    expect(domainEvent.name()).toEqual(eventName);
    expect(domainEvent.occurredOn()).toEqual(timestamp);
  });
});
