import EventA from './data/EventA';
import DomainEventPublisher from '../../../../../src/domain/SharedKernel/Event/DomainEventPublisher';
import TestSubscriber, { mockHandleEvent, mockSubscribedToEventType } from './data/TestSubscriber';

describe('DomainEventPublisher unit test', () => {
  const domainEvent = new EventA('111-222-333', 'test title');

  beforeEach(() => {
    DomainEventPublisher.instance().subscribe(new TestSubscriber());
  });

  afterEach(() => {
    DomainEventPublisher.instance().reset();
    mockHandleEvent.mockClear();
    mockSubscribedToEventType.mockClear();
  });

  it('should publish event', async () => {
    DomainEventPublisher.instance().publish(domainEvent);

    expect(mockHandleEvent).toBeCalledTimes(1);
    expect(mockHandleEvent).toBeCalledWith(domainEvent);
    expect(mockSubscribedToEventType).toBeCalledTimes(1);
  });

  it('should publish several events', async () => {
    DomainEventPublisher.instance().publishAll([domainEvent, domainEvent]);

    expect(mockHandleEvent).toBeCalledTimes(2);
    expect(mockHandleEvent).toBeCalledWith(domainEvent);
    expect(mockSubscribedToEventType).toBeCalledTimes(2);
  });
});
