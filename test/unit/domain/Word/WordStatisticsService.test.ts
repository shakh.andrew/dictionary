import WordStatisticsService from '../../../../src/domain/Word/WordStatisticsService';
import { WordTest } from '../../application/__mocks__/WordTest';

describe('WordStatisticsService unit test', () => {
  let wordStatisticsService: WordStatisticsService = null;

  beforeEach(() => {
    wordStatisticsService = new WordStatisticsService();
  });

  it('should return correct data', async () => {
    const words = [WordTest.getWord('1'), WordTest.getWord('2'), WordTest.getWord('3')];
    const data = await wordStatisticsService.getStatistics(words);

    expect(data.countOfWords).toEqual(3);
    expect(data.countOfLetters).toEqual(9);
  });
});
