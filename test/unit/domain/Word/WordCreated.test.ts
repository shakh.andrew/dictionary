import WordCreated from '../../../../src/domain/Word/WordCreated';

describe('WordCreated unit test', () => {
  let event: WordCreated = null;
  const id = '111-222-333';
  const origin = 'dog';

  beforeEach(() => {
    event = WordCreated.create(id, origin);
  });

  it('should create right object', async () => {
    expect(event.getId()).toEqual(id);
    expect(event.getOrigin()).toEqual(origin);
  });
});
