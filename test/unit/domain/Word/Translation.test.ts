import Translation from '../../../../src/domain/Word/Translation';
import payload from '../data';

describe('Translation unit test', () => {
  let translation: Translation = null;
  const word = payload.word.translations[0].word;
  const lang = payload.word.translations[0].lang;

  beforeEach(() => {
    translation = Translation.create(word, lang);
  });

  it('should create right object', async () => {
    expect(translation.getLang()).toEqual(lang);
    expect(translation.getWord()).toEqual(word);
  });

  it('should right compare two values', async () => {
    const copyTranslation = Translation.create(word, lang);
    const anotherTranslation = Translation.create('word1', lang);

    expect(translation.equals(copyTranslation)).toBeTruthy();
    expect(translation.equals(anotherTranslation)).toBeFalsy();
    expect(translation.equals(undefined)).toBeFalsy();
  });

  it('should throw error (invalid domain object)', async () => {
    expect(() => {
      Translation.create(null, lang);
    }).toThrowError('Translation word should not be empty');

    expect(() => {
      Translation.create(word, 'de');
    }).toThrowError(`Only allowed following languages: ${Translation.allowedLanguage.join(',')}`);
  });
});
