import Word from '../../../../src/domain/Word/Word';
import Translation from '../../../../src/domain/Word/Translation';
import payload from '../data';

describe('Word unit test', () => {
  let word: Word = null;
  const id = payload.word.id;
  const origin = payload.word.origin;
  const translationUk = Translation.create(payload.word.translations[0].word, payload.word.translations[0].lang);
  const translationFr = Translation.create(payload.word.translations[1].word, payload.word.translations[1].lang);
  const translations = [translationUk, translationFr];

  beforeEach(() => {
    word = Word.create(id, origin, translations);
  });

  it('should create right object', async () => {
    expect(word.getId()).toEqual(id);
    expect(word.getOrigin()).toEqual(origin);
    expect(word.getTranslations()).toEqual(translations);
    expect(word.getTranslation('uk')).toEqual(translationUk.getWord());
  });

  it('should return right count of letters of origin', async () => {
    expect(word.countOfLetters()).toEqual(origin.length);
  });

  it('should throw error (invalid domain object)', async () => {
    expect(() => {
      Word.create(null, origin, translations);
    }).toThrowError('id should not be empty');

    expect(() => {
      Word.create(id, undefined, translations);
    }).toThrowError('origin should not be empty');

    expect(() => {
      Word.create(id, origin, []);
    }).toThrowError('translations should not be empty array');
  });
});
