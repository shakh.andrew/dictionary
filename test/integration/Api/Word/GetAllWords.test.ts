import request from 'supertest';
import { Application } from 'express';
import App from '../../../../src/infrastructure/Api/App';
import Translation from '../../../../src/domain/Word/Translation';
import Word from '../../../../src/domain/Word/Word';

jest.mock('../../../../src/infrastructure/Word/WordRepository', () => {
  return jest.fn().mockImplementation(() => {
    const translation1 = Translation.create('ручка', 'uk');
    const translation2 = Translation.create('ручка2', 'fr');
    const word = Word.create('1', 'pen', [translation1, translation2]);

    return { getBy: jest.fn(() => Promise.resolve([word])) };
  });
});

describe('Get all word API', () => {
  let app: Application;

  beforeAll(async () => {
    app = await App.getApplication();
  });

  it('should returns all words', async () => {
    const result = await request(app).get('/api/words?language=uk');

    expect(result.status).toEqual(200);
    expect(result.body).toBeInstanceOf(Array);
    result.body.forEach((e: any) => {
      expect(e.origin).toBeDefined();
      expect(e.translation).toBeDefined();
    });
    expect(result.body).toEqual([{ origin: 'pen', translation: 'ручка' }]);
  });

  it('should throw validation error', async () => {
    const result = await request(app).get('/api/words?language=UndefinedDay');

    expect(result.status).toEqual(400);
    expect(result.body.error).toEqual('"value" must be one of [es, uk, fr]');
  });
});
