import request from 'supertest';
import { Application } from 'express';
import App from '../../../../src/infrastructure/Api/App';
import Translation from '../../../../src/domain/Word/Translation';
import Word from '../../../../src/domain/Word/Word';
import WordCreated from '../../../../src/domain/Word/WordCreated';
import Helper from '../../../Helper';

const translation = Translation.create('ручка-123', 'uk');
const word = Word.create('1234567890', 'pen', [translation]);

const mockAdd = jest.fn(() => Promise.resolve());
const mockTranslate = jest.fn(() => Promise.resolve(translation));
const mockHandleEvent = jest.fn(() => Promise.resolve());

jest.mock('../../../../src/infrastructure/Word/WordRepository', () => {
  return jest.fn().mockImplementation(() => {
    return {
      getNextId: jest.fn(() => '1234567890'),
      add: mockAdd,
    };
  });
});

jest.mock('../../../../src/infrastructure/Word/Translator/Provider/Google/GoogleTranslatorClient', () => {
  return jest.fn().mockImplementation(() => {
    return {
      translate: mockTranslate,
    };
  });
});

jest.mock('../../../../src/application/Word/DomainSubscriber/WordLogger', () => {
  return jest.fn().mockImplementation(() => {
    return {
      handleEvent: mockHandleEvent,
      subscribedToEventType: jest.fn(() => 'WordCreated'),
    };
  });
});

describe('Create new word API', () => {
  let app: Application;

  beforeAll(async () => {
    app = await App.getApplication();
    Helper.mockDate(2022, 1, 18);
  });

  afterAll(() => {
    Helper.reset();
  });

  it('should returns all words', async () => {
    const result = await request(app).post('/api/words').send({
      origin: 'pen',
      target: 'uk',
    });

    expect(result.status).toEqual(200);
    expect(result.body).toEqual({});
    expect(mockAdd).toBeCalled();
    expect(mockAdd).toBeCalledWith(word);
    expect(mockTranslate).toBeCalled();
    expect(mockTranslate).toBeCalledWith('pen', 'en', 'uk');
    expect(mockHandleEvent).toBeCalled();
    expect(mockHandleEvent).toBeCalledWith(WordCreated.create('1234567890', 'pen'));
  });

  it('should throw validation error', async () => {
    const result = await request(app).post('/api/words').send({
      origin: 'pen',
    });

    expect(result.status).toEqual(400);
    expect(result.body.error).toEqual('"target" is required');
  });
});
