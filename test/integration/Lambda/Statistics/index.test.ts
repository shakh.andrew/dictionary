import Translation from '../../../../src/domain/Word/Translation';
import Word from '../../../../src/domain/Word/Word';

const lambda = require('../../../../src/infrastructure/Lambda/Statistics/index');

jest.mock('../../../../src/infrastructure/Word/WordRepository', () => {
  return jest.fn().mockImplementation(() => {
    const translation1 = Translation.create('ручка', 'uk');
    const translation2 = Translation.create('ручка2', 'fr');
    const word = Word.create('1', 'pen', [translation1, translation2]);

    return { getBy: jest.fn(() => Promise.resolve([word])) };
  });
});

describe('Lambda get word statistics integration test', () => {
  it('should returns correct statistics data', async () => {
    const result = await lambda.handler();

    expect(result).toEqual({
      countOfLetters: 3,
      countOfWords: 1,
    });
  });
});
