import Helper from './Helper';

describe('Helper unit test', () => {
  afterAll(() => {
    Helper.reset();
  });

  it('should mock date', async () => {
    Helper.mockDate();
    expect(Date.now()).toEqual(1605398400000);

    Helper.reset();

    Helper.mockDate(1989, 4, 2);
    expect(Date.now()).toEqual(607478400000);
  });

  it('should clone object', async () => {
    const object = { test: { qwerty: 12345 } };
    const clone = Helper.clone(object);

    expect(object).toEqual(clone);
    expect(clone === object).toBeFalsy();
  });

  it('should mock random function', async () => {
    Helper.mockRandom();
    expect(Math.random()).toEqual(0.123456789);
    expect(Math.random()).toEqual(0.123456789);

    Helper.mockRandom(13.07);
    expect(Math.random()).toEqual(13.07);
    expect(Math.random()).toEqual(13.07);

    Helper.reset();
    expect(Math.random() === 13.07).toBeFalsy();
  });
});
