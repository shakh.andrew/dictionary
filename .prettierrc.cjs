module.exports = {
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  bracketSpacing: true,
  arrowParens: 'always',
  tabWidth: 2,
  semi: true,
  semicolons: true,
  printWidth: 140,
  endOfLine: 'auto',
};
